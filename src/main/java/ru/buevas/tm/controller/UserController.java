package ru.buevas.tm.controller;

import java.io.IOException;
import java.util.List;
import ru.buevas.tm.entity.User;
import ru.buevas.tm.enumerated.Role;
import ru.buevas.tm.service.UserService;

/**
 * Контроллер для операций над пользователями
 *
 * @author Andrey Buev
 */
public class UserController extends AbstractController{

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Обработка команды аутентификации пользователя
     */
    public Long signIn() throws IOException {
        System.out.println("[SIGN IN]");
        System.out.print("Enter login: ");
        final String login = reader.readLine();

        System.out.print("Enter password: ");
        final String password = reader.readLine();

        final User user = userService.authenticate(login, password);
        if (user == null) printError("Incorrect login or password");
        System.out.println("[SUCCESS]");
        return user.getId();
    }

    /**
     * Обработка команды завершения пользовательской сессии
     */
    public Long logout(final Long currentUserId) {
        System.out.println("[LOGOUT]");
        if (currentUserId == null) printError("Error on logout");
        System.out.println("[SUCCESS]");
        return null;
    }

    /**
     * Обработка команды смены пароля пользователя
     * @param currentUserId
     */
    public void changePassword(final Long currentUserId) throws IOException{
        System.out.println("[CHANGE_PASSWORD]");
        if (currentUserId == null) {
            System.out.println("To change password you need to login");
            return;
        }
        final User user = userService.findById(currentUserId);
        if (user.getRole() == Role.ADMIN) {
            changePasswordAsAdmin();
        } else {
            changePasswordAsUser(currentUserId);
        }
        System.out.println("[SUCCESS]");
    }

    private void changePasswordAsAdmin() throws IOException {
        printUserList();
        System.out.print("Select a user to change the password (enter the number from the list): ");
        final int index = Integer.parseInt(reader.readLine()) - 1;
        System.out.print("Enter password: ");
        final String newPassword = reader.readLine();

        final User user = userService.findByIndex(index);
        if (user == null) return;
        userService.update(user.getId(), user.getLogin(), newPassword, user.getRole());
    }

    private void changePasswordAsUser(Long currentUserId) throws IOException {
        System.out.print("Enter password: ");
        final String newPassword = reader.readLine();
        final User user = userService.findById(currentUserId);
        if (user == null) return;
        userService.update(user.getId(), user.getLogin(), newPassword, user.getRole());
    }

    /**
     * Обработка команды создания пользователя
     */
    public void createUser() throws IOException {
        System.out.println("[CREATE USER]");

        System.out.print("Enter login: ");
        final String login = reader.readLine();

        System.out.print("Enter password: ");
        final String password = reader.readLine();

        Role role = selectRole();
        if (role == null) printError("Selected role not exists");
        userService.create(login, password, role);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды вывода списка пользователей
     */
    public void listUser() {
        System.out.println("[LIST USER]");
        printUserList();
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды обновления пользователя по номеру в списке
     */
    public void updateUserByIndex() throws IOException{
        System.out.println("[UPDATE USER]");

        System.out.print("Enter user index: ");
        final int index = Integer.parseInt(reader.readLine()) - 1;
        final User user = userService.findByIndex(index);
        if (user == null) printError("User not found");

        System.out.print("Enter login: ");
        final String login = reader.readLine();

        System.out.print("Enter password: ");
        final String password = reader.readLine();

        Role role = selectRole();
        if (role == null) printError("Selected role not exists");

        userService.update(user.getId(), login, password, role);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды обновления пользователя по идентификатору
     */
    public void updateUserById() throws IOException {
        System.out.println("[UPDATE USER]");

        System.out.print("Enter user id: ");
        final Long id = Long.parseLong(reader.readLine());
        final User user = userService.findById(id);
        if (user == null) printError("User not found");

        System.out.print("Enter login: ");
        final String login = reader.readLine();

        System.out.print("Enter password: ");
        final String password = reader.readLine();

        Role role = selectRole();
        if (role == null) printError("Selected role not exists");

        userService.update(user.getId(), login, password, role);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды отображения пользователя по номеру в списке
     */
    public void viewUserByIndex() throws IOException {
        System.out.print("Enter index: ");
        final int index = Integer.parseInt(reader.readLine()) - 1;
        final User user = userService.findByIndex(index);
        if (user != null) printUser(user);
    }

    /**
     * Обработка команды отображения пользователя по идентификатору
     */
    public void viewUserById() throws IOException {
        System.out.print("Enter id: ");
        final Long id = Long.parseLong(reader.readLine());
        final User user = userService.findById(id);
        if (user != null) printUser(user);
    }

    /**
     * Обработка команды отображения пользователя по логину
     */
    public void viewTaskByLogin() throws IOException {
        System.out.print("Enter name: ");
        final String login = reader.readLine();
        final User user = userService.findByLogin(login);
        if (user != null) printUser(user);
    }

    /**
     * Обработка команды удаления пользователя по номеру в списке
     */
    public void removeUserByIndex() throws IOException {
        System.out.println("[REMOVE USER BY INDEX]");
        System.out.print("Enter index: ");
        final int index = Integer.parseInt(reader.readLine()) - 1;
        final User user = userService.removeByIndex(index);
        if (user != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Removed user not found");
        }
    }

    /**
     * Обработка команды удаления пользователя по идентификатору
     */
    public void removeUserById() throws IOException {
        System.out.println("[REMOVE USER BY ID]");
        System.out.print("Enter id: ");
        final Long id = Long.parseLong(reader.readLine());
        final User user = userService.removeById(id);
        if (user != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Removed user not found");
        }
    }

    /**
     * Обработка команды удаления пользователя по логину
     */
    public void removeUserByLogin() throws IOException {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.print("Enter login: ");
        final String login = reader.readLine();
        final User user = userService.removeByLogin(login);
        if (user != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Removed user not found");
        }
    }

    /**
     * Выбор пользователем роли
     *
     * @return выбранная роль
     * @throws IOException
     */
    private Role selectRole() throws IOException {
        System.out.print("Select role (1 - User, 2 - Admin): ");
        final String roleSelection = reader.readLine();
        Role role;
        switch (roleSelection) {
            case "1": return Role.USER;
            case "2": return role = Role.ADMIN;
            default: return null;
        }
    }

    private void printUserList() {
        System.out.println("Available users:");
        final List<User> users = userService.findAll();
        if (users == null) return;
        if (users.isEmpty()) {
            System.out.println("Empty");
        } else {
            int index = 1;
            for (User user : users) {
                System.out.printf("%d. %s - %s (%s)%n", index, user.getId(), user.getLogin(), user.getRole().getDisplayName());
                index++;
            }
        }
    }

    /**
     * Вывод на экран информации о пользователе
     *
     * @param user задача
     * @see User
     */
    private void printUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("id: " + user.getId());
        System.out.println("login: " + user.getLogin());
        System.out.println("Role: " + user.getRole().getDisplayName());
        System.out.println("[SUCCESS]");
    }
}
