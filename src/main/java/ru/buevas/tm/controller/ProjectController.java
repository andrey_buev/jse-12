package ru.buevas.tm.controller;

import java.io.IOException;
import java.util.List;
import ru.buevas.tm.entity.Project;
import ru.buevas.tm.entity.Task;
import ru.buevas.tm.service.ProjectService;

/**
 * Контроллер для операций над проектами
 *
 * @author Andrey Buev
 */
public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Обработка команды создания проекта
     */
    public void createProject() throws IOException {
        System.out.println("[CREATE PROJECT]");

        System.out.print("Enter project name: ");
        final String name = reader.readLine();

        System.out.print("Enter project description: ");
        final String description = reader.readLine();

        projectService.create(name, description);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды вывода списка проектов
     */
    public void listProject() {
        System.out.println("[LIST PROJECT]");

        System.out.println("Available projects:");
        final List<Project> projects = projectService.findAll();
        printProjectList(projects);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды обновления проекта по номеру в списке
     */
    public void updateProjectByIndex() throws IOException {
        System.out.println("[Update PROJECT]");

        System.out.print("Enter project index: ");
        final int index = Integer.parseInt(reader.readLine()) - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) printError("Project not found");

        System.out.print("Enter project name: ");
        final String name = reader.readLine();

        System.out.print("Enter project description: ");
        final String description = reader.readLine();

        projectService.update(project.getId(), name, description);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды обновления проекта по идентификатору
     */
    public void updateProjectById() throws IOException {
        System.out.println("[Update PROJECT]");

        System.out.print("Enter project id: ");
        final Long id = Long.parseLong(reader.readLine());
        final Project project = projectService.findById(id);
        if (project == null) printError("Project not found");

        System.out.print("Enter project name: ");
        final String name = reader.readLine();

        System.out.print("Enter project description: ");
        final String description = reader.readLine();

        projectService.update(project.getId(), name, description);
        System.out.println("[SUCCESS]");
    }

    /**
     * Вывод на экран информации о проекте
     *
     * @param project проект
     * @see Project
     */
    private void printProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("id: " + project.getId());
        System.out.println("name: " + project.getName());
        System.out.println("description: " + project.getDescription());
        System.out.println("[SUCCESS]");
    }

    /**
     * Вывод на экран коллекции проектов
     *
     * @param projects коллекция проектов
     * @see Task
     */
    private void printProjectList(final List<Project> projects) {
        if (projects == null) return;
        if (projects.isEmpty()) {
            System.out.println("Empty");
        } else {
            int index = 1;
            for (Project project : projects) {
                System.out.printf("%d. %s - %s%n", index, project.getId(), project.getName());
                index++;
            }
        }
    }

    /**
     * Обработка команды отображения проекта по номеру в списке
     */
    public void viewProjectByIndex() throws IOException {
        System.out.print("Enter index: ");
        final int index = Integer.parseInt(reader.readLine()) - 1;

        final Project project = projectService.findByIndex(index);
        if (project != null) printProject(project);
    }

    /**
     * Обработка команды отображения проекта по идентификатору
     */
    public void viewProjectById() throws IOException {
        System.out.print("Enter id: ");
        final Long id = Long.parseLong(reader.readLine());

        final Project project = projectService.findById(id);
        if (project != null) printProject(project);
    }

    /**
     * Обработка команды отображения проекта по имени
     */
    public void viewProjectByName() throws IOException {
        System.out.print("Enter name: ");
        final String name = reader.readLine();

        final Project project = projectService.findByName(name);
        if (project != null) printProject(project);
    }

    /**
     * Обработка команды удаления проекта по номеру в списке
     */
    public void removeProjectByIndex() throws IOException {
        System.out.println("[REMOVE PROJECT BY INDEX]");

        System.out.print("Enter index: ");
        final int index = Integer.parseInt(reader.readLine()) - 1;

        final Project project = projectService.removeByIndex(index);
        if (project != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Removed project not found");
        }
    }

    /**
     * Обработка команды удаления проекта по идентификатору
     */
    public void removeProjectById() throws IOException {
        System.out.println("[REMOVE PROJECT BY ID]");

        System.out.print("Enter id: ");
        Long id = Long.parseLong(reader.readLine());

        Project project = projectService.removeById(id);
        if (project != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Removed project not found");
        }
    }

    /**
     * Обработка команды удаления проекта по имени
     */
    public void removeProjectByName() throws IOException {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.print("Enter name: ");
        final String name = reader.readLine();
        final Project project = projectService.removeByName(name);
        if (project != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Removed project not found");
        }
    }

    /**
     * Обработка команды отчистки списка проектов
     */
    public void clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[SUCCESS]");
    }
}
