package ru.buevas.tm.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Базовый контроллер
 *
 * @author Andrey Buev
 */
public abstract class AbstractController {

    protected final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Вывод на экран сообщения об ошибке
     *
     * @param message сообщение
     */
    public void printError(final String message) {
        System.out.println("[ERROR]");
        System.out.println(message);
        exit(-1);
    }

    /**
     * Завершение работы приложения
     */
    public void exit(final int code) {
        System.exit(code);
    }
}
