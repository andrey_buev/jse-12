package ru.buevas.tm.service;

import java.util.List;
import ru.buevas.tm.entity.Project;
import ru.buevas.tm.repository.ProjectRepository;

/**
 * Сервис проектов
 *
 * Делегирует операции над проектами репозиторию с предшествующей обработкой и валидацией поступающей информации
 *
 * @author Andrey Buev
 * @see ProjectRepository
 */
public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null) return null;
        return projectRepository.create(name, description);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null) return null;
        return projectRepository.update(id, name, description);
    }

    public Project findByIndex(final int index) {
        return projectRepository.findByIndex(index);
    }

    public Project findById(final Long id) {
        return projectRepository.findById(id);
    }

    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public Project removeByIndex(final int index) {
        return projectRepository.removeByIndex(index);
    }

    public Project removeById(final Long id) {
        return projectRepository.removeById(id);
    }

    public Project removeByName(final String name) {
        return projectRepository.removeByName(name);
    }

    public void clear() {
        projectRepository.clear();
    }
}
