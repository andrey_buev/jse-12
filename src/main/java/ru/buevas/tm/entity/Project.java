package ru.buevas.tm.entity;

/**
 * Сущность для хранения инфомации о проекте
 */
public class Project {

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    /**
     * Конструктор по умолчанию
     */
    public Project() {
    }

    /**
     * Конструктор по имени проекта
     *
     * @param name наименование создаваемого проекта
     */
    public Project(final String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("{Project %d - %s}", id, name);
    }
}
